class LoginPage
  include Capybara::DSL

  def client_login(email, pwd)
    find("#username").set email
    find("#password").set pwd
    find("#login-button").click
  end
end
