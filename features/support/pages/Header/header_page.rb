class HeaderPage
  include Capybara::DSL

  def link_entrar
    find('a[qa-automation="home-account-button"]')
  end

  def link_login
    find("ul li a", text: "Login")
  end
end
