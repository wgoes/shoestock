class CartPage
  include Capybara::DSL

  def title_page
    find("h1", text: "Meu carrinho")
  end

  def continue_button
    find('a[qa-auto="cart-buy-button"]')
  end
end
