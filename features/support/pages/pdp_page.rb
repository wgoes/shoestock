class PDPPage
  include Capybara::DSL

  def access
    visit "https://www.shoestock.com.br/bota-coturno-shoestock-couro-fivelas-feminina-preto-O01-3607-006"
  end

  def select_size(size)
    find(".product-item", text: size).click
  end

  def buy
    find("#buy-button-now")
  end

  def get_short_description
    find(".short-description").text
  end
end
