class BuscaPage
  include Capybara::DSL

  def label_resultado
    find(".search-query")
  end
end
