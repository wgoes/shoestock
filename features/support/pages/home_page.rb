class HomePage
  include Capybara::DSL

  def access
    visit "https://www.shoestock.com.br/"
  end

  def search
    find("#search-input")
  end

  def icon_lupa
    find("button[title=Buscar]")
  end

  def select_product
    search.click
    search.set "Tenis"
    page.has_css?(".name")
    product = all(".name", text: "Tênis Couro Shoestock Basic Comfy Feminino")
    product[0].click
  end

  def aguardar_carregar_home
    page.has_css?(".banner-stripe-home")
  end
end
