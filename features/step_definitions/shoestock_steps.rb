$short_description
$home = HomePage.new
$busca = BuscaPage.new
$pdp = PDPPage.new
$header = HeaderPage.new
$cart = CartPage.new
$login = LoginPage.new

Dado("que o cliente está na home do site.") do
  $home.access
  $home.aguardar_carregar_home
end

Quando("pesquisar pelo termo {string}.") do |term|
  $home.search.set term
  $home.icon_lupa.click
end

Então("deve visualizar a tela de resultado de busca e o texto {string}.") do |expected_msg|
  expect($busca.label_resultado.text).to eql expected_msg
end

Dado("que o cliente acessou o detalhe de um produto.") do
  $pdp.access
end

Quando("selecionar o tamanho.") do
  $pdp.select_size("34")
end

Quando("clicar em comprar.") do
  $pdp.buy.click
end

Então("o cliente deve ser redirecionado para o carrinho.") do
  $cart = CartPage.new
  expect($cart.title_page.text).to eql "Meu carrinho"
end

Dado("que o cliente está autenticado no site.") do
  $home.access
  $home.aguardar_carregar_home
  $header.link_entrar.hover
  $header.link_login.click
  $login.client_login("goes.william@gmail.com", "123456")
  $home.aguardar_carregar_home
end

Dado("que adicionou somente o produto xpto no carrinho.") do
  $home.select_product
end

Quando("chegar na tela de pagamento.") do
  $short_description = $pdp.get_short_description
  $pdp.select_size("35")
  $pdp.buy.click
  $cart.continue_button.click
end

Então("deve visualizar somente o produto xpto na lista de produtos incluídos.") do
  product_name = "Tênis Couro Shoestock Basic Comfy Feminino"
  expect($short_description).to have_content product_name.upcase
end
