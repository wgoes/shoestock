#language: pt

@todos
Funcionalidade: Shoestock

@realizar_busca
Cenário: Realizar busca
Dado que o cliente está na home do site.
Quando pesquisar pelo termo "Tênis".
Então deve visualizar a tela de resultado de busca e o texto 'RESULTADOS DE BUSCA PARA "TÊNIS"'.

@incluir_produto_carrinho
Cenario: Incluir produto no carrinho.
Dado que o cliente acessou o detalhe de um produto.
Quando selecionar o tamanho.
E clicar em comprar.
Então o cliente deve ser redirecionado para o carrinho.

@validar_produtos_carrinho_pgto
Cenário: Validar os produtos incluídos no carrinho na tela de pagamento.
Dado que o cliente está autenticado no site.
E que adicionou somente o produto xpto no carrinho.
Quando chegar na tela de pagamento.
Então deve visualizar somente o produto xpto na lista de produtos incluídos.